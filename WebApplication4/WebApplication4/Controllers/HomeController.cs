﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkında Sayfası";

            return View();
        }

        public ActionResult Giris()
        {
            ViewBag.Message = "Giriş Sayfası";

            return View();
        }
        public ActionResult KayitOl()
        {
            ViewBag.Message = "Kayıt Sayfası";

            return View();
        }
    }
}